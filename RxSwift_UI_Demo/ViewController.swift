//
//  ViewController.swift
//  RxSwift_UI_Demo
//
//  Created by Victor Kachalov on 03/03/2018.
//  Copyright © 2018 Victor Kachalov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    //TapGestureRecognizer
    @IBOutlet weak var tapGestureRecognizer: UITapGestureRecognizer!
    
    //Button and Label
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var buttonLabel: UILabel!
    
    //Slider and ProgressView
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var progressView: UIProgressView!
    
    //SegmentedControl and Label
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlLabel: UILabel!
    
    //DatePicker and Label
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerLabel: UILabel!
    
    //TextField and Label
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textFieldLabel: UILabel!
    
    //TextView and Label
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewLabel: UILabel!
    
    //Switch and ActivityIndicator
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //Stepper and Label
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var stepperLabel: UILabel!
    
    
    //DisposeBag - в этом примере он объявлен как свойство. Это нужно, чтобы во время вызова системой deinit, происходило освобождение ресурсов для Observable-объектов.
    
    let disposeBag = DisposeBag()
    
    //DateFormatt for DatePicker
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tapGestureRecognizer.rx.event.asDriver().drive(onNext: { [unowned self] _ in
            self.view.endEditing(true)
        }).disposed(by: disposeBag)
        
        textField.rx.text.bind {
            self.textFieldLabel.text = $0
            }.disposed(by: disposeBag)
        
        textView.rx.text.bind {
            self.textViewLabel.text = "Character count: \(String(describing: $0?.count))"
            }.disposed(by: disposeBag)
        
        button.rx.tap.asDriver().drive(onNext: {
            self.buttonLabel.text! += "Hello, RxSwift"
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }).disposed(by: disposeBag)
        
        slider.rx.value.asDriver().drive(progressView.rx.progress).disposed(by: disposeBag)
        
        
        segmentedControl.rx.value.asDriver().skip(1).drive(onNext: {
            self.segmentedControlLabel.text = "Selected segment = \($0)"
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }).disposed(by: disposeBag)
        
        datePicker.rx.date.asDriver()
            .map {
                self.dateFormatter.string(from: $0)
            }.drive(onNext: {
                self.datePickerLabel.text = "Selected date: \($0)"
                
            }).disposed(by: disposeBag)
        
        stepper.rx.value.asDriver().map { String(Int($0)) }.drive(stepperLabel.rx.text).disposed(by: disposeBag)
        
        mySwitch.rx.value.asDriver().map { !$0 }.drive(activityIndicator.rx.isHidden).disposed(by: disposeBag)
        
        mySwitch.rx.value.asDriver().drive(activityIndicator.rx.isAnimating).disposed(by: disposeBag)
    }
    
}
